package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.TeacherDao;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.entity.form.UpdateProfileForm;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.UserRepository;

import java.util.List;

@Service
@Slf4j
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    TeacherDao teacherDao;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<Teacher> getAllTeacher() {
        log.info("teacher service received called");
        List<Teacher> teachers = teacherDao.getAllTeacher();
        log.info("teacher service received {} \n from dao", teachers);
        return teachers;
    }

    @Override
    public Teacher getTeacherById(Long id) {
        log.info("teacher service received called");
        Teacher teacher = teacherDao.getTeacherById(id);
        log.info("teacher service received {} \n from dao", teacher);
        return teacher;
    }

    @Override
    public Teacher updateProfile(UpdateProfileForm updateProfileForm) {
        log.info("update profile teacher service is called");
        Teacher teacher = teacherDao.getTeacherById(updateProfileForm.getAppUserId());
        if (!updateProfileForm.getImage().equals("")) {
            teacher.setImage(updateProfileForm.getImage());
        }
        if (!updateProfileForm.getNewPassword().equals("")) {
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            User user = userRepository.findByAppUserId(teacher.getId());
            user.setPassword(encoder.encode(updateProfileForm.getNewPassword()));
            userRepository.save(user);
        }
        teacher = teacherDao.saveTeacher(teacher);
        return teacher;
    }
}
