package se331.lab.rest.service;

import se331.lab.rest.entity.Teacher;
import se331.lab.rest.entity.form.UpdateProfileForm;

import java.util.List;

public interface TeacherService {
    List<Teacher> getAllTeacher();

    Teacher getTeacherById(Long id);

    Teacher updateProfile(UpdateProfileForm updateProfileForm);
}
