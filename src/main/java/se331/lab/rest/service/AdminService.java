package se331.lab.rest.service;

import se331.lab.rest.entity.Admin;
import se331.lab.rest.entity.form.UpdateProfileForm;

import java.util.List;

public interface AdminService {
    List<Admin> getAllAdmin();

    Admin getAdminById(Long id);

    Admin updateProfile(UpdateProfileForm updateProfileForm);
}
