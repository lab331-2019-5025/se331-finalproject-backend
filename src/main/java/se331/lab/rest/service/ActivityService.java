package se331.lab.rest.service;

import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.form.AddAndUpdateActivityForm;
import se331.lab.rest.entity.form.EnrollActForm;

import java.util.List;

public interface ActivityService {
    List<Activity> getAllActivity();

    Activity getActivityById(Long id);

    Activity saveActivity(AddAndUpdateActivityForm actForm);

    Activity updateActivity(AddAndUpdateActivityForm actForm);

    Activity enrollActivity(EnrollActForm form);

    Activity confirmEnrollActivity(EnrollActForm form);

    Activity rejectEnrollActivity(EnrollActForm form);
}
