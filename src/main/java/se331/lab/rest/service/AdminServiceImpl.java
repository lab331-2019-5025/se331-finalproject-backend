package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.AdminDao;
import se331.lab.rest.entity.Admin;
import se331.lab.rest.entity.form.UpdateProfileForm;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.UserRepository;

import java.util.List;

@Service
@Slf4j
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminDao adminDao;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<Admin> getAllAdmin() {
        log.info("admin service received called");
        List<Admin> admins = adminDao.getAllAdmin();
        log.info("admin service received {} \n from dao", admins);
        return admins;
    }

    @Override
    public Admin getAdminById(Long id) {
        log.info("admin service received called");
        Admin admin = adminDao.getAdminById(id);
        log.info("admin service received {} \n from dao", admin);
        return admin;
    }

    @Override
    public Admin updateProfile(UpdateProfileForm updateProfileForm) {
        Admin admin = adminDao.getAdminById(updateProfileForm.getAppUserId());
        if (!updateProfileForm.getImage().equals("")) {
            admin.setImage(updateProfileForm.getImage());
        }
        if (!updateProfileForm.getNewPassword().equals("")) {
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            User user = userRepository.findByAppUserId(admin.getId());
            user.setPassword(encoder.encode(updateProfileForm.getNewPassword()));
            userRepository.save(user);
        }
        admin = adminDao.saveAdmin(admin);
        return admin;
    }
}
