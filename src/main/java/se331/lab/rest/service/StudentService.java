package se331.lab.rest.service;

import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.form.AddStudentForm;
import se331.lab.rest.entity.form.UpdateProfileForm;

import java.util.List;

public interface StudentService {
    List<Student> getAllStudent();

    Student getStudentById(Long id);

    Student saveStudent(AddStudentForm stuForm);

    String removeStudent(Long id);

    Student confirmRegisStudent(Long id);

    Student updateProfile(UpdateProfileForm updateProfileForm);
}
