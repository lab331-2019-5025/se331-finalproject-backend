package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.form.AddStudentForm;
import se331.lab.rest.entity.form.UpdateProfileForm;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class StudentServiceImpl implements StudentService {
    @Autowired
    StudentDao studentDao;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<Student> getAllStudent() {
        log.info("student service received called");
        List<Student> students = studentDao.getAllStudent();
        log.info("student service received {} \n from dao", students);
        return students;
    }

    @Override
    public Student getStudentById(Long id) {
        log.info("student service received called");
        Student student = studentDao.getStudentById(id);
        log.info("student service received {} \n from dao", student);
        return student;
    }

    @Override
    public Student saveStudent(AddStudentForm stuForm) {
        Student student = Student.builder()
                .name(stuForm.getName())
                .surname(stuForm.getSurname())
                .image(stuForm.getImage())
                .studentId(stuForm.getStudentId())
                .dob(stuForm.getDob())
                .major(stuForm.getMajor())
                .year(stuForm.getYear())
                .email(stuForm.getEmail())
                .confirmed(false)
                .build();
        student = studentDao.saveStudent(student);

        PasswordEncoder encoder = new BCryptPasswordEncoder();
        User user = User.builder()
                .username(stuForm.getEmail())
                .password(encoder.encode(stuForm.getPassword()))
                .firstname(stuForm.getName())
                .lastname(stuForm.getSurname())
                .email(stuForm.getEmail())
                .enabled(false)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();
        user.getAuthorities().add(authorityRepository.findByName(AuthorityName.ROLE_STUDENT));
        userRepository.save(user);
        student.setUser(user);
        user.setAppUser(student);

        userRepository.save(user);
        student = studentDao.saveStudent(student);
        return student;
    }

    @Override
    public String removeStudent(Long id) {
        log.info("student service remove student id {}", id);
        return studentDao.removeStudent(id);
    }

    @Override
    public Student confirmRegisStudent(Long id) {
        log.info("student service confirm student id {}", id);
        Student student = studentDao.getStudentById(id);
        student.setConfirmed(true);
        studentDao.saveStudent(student);
        User user = userRepository.findByAppUserId(student.getId());
        user.setEnabled(true);
        userRepository.save(user);
        return student;
    }

    @Override
    public Student updateProfile(UpdateProfileForm updateProfileForm) {
        log.info("update profile student service is called");
        Student student = studentDao.getStudentById(updateProfileForm.getAppUserId());
        if (!updateProfileForm.getImage().equals("")) {
            student.setImage(updateProfileForm.getImage());
        }
        if (!updateProfileForm.getNewPassword().equals("")) {
            PasswordEncoder encoder = new BCryptPasswordEncoder();
            User user = userRepository.findByAppUserId(student.getId());
            user.setPassword(encoder.encode(updateProfileForm.getNewPassword()));
            userRepository.save(user);
        }
        student = studentDao.saveStudent(student);
        return student;
    }
}
