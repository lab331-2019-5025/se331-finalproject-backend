package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.ActivityDao;
import se331.lab.rest.dao.CommentDao;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.entity.form.AddCommentForm;
import se331.lab.rest.security.repository.UserRepository;

import java.util.List;

@Service
@Slf4j
public class CommentServiceImpl implements CommentService {
    @Autowired
    CommentDao commentDao;

    @Autowired
    ActivityDao activityDao;

    @Autowired
    UserRepository userRepository;


    @Override
    public List<Comment> getAllComment() {
        log.info("get all comment service received called");
        List<Comment> comments = commentDao.getAllComment();
        log.info("comment service received {} \n from dao", comments);
        return comments;
    }

    @Override
    public Comment getCommentById(Long id) {
        log.info("get comment by id service received called");
        Comment comment = commentDao.getCommentById(id);
        log.info("comment service received {} \n from dao", comment);
        return comment;
    }

    @Override
    public Comment saveComment(AddCommentForm commentForm) {
        log.info(" save comment service received called");
        Comment comment = Comment.builder()
                .message(commentForm.getMessage())
                .timestamp(commentForm.getTimestamp())
                .image(commentForm.getImage())
                .activity(this.activityDao.getActivityById(commentForm.getActivityId()))
                .author(this.userRepository.findByAppUserId(commentForm.getUserId()).getAppUser())
                .build();
        return commentDao.saveComment(comment);
    }

    @Override
    public String removeComment(Long id) {
        log.info("remove comment service received called");
        commentDao.removeComment(id);
        return "delete comment";
    }
}
