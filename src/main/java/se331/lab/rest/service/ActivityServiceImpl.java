package se331.lab.rest.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.ActivityDao;
import se331.lab.rest.dao.StudentDao;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.entity.Student;
import se331.lab.rest.entity.form.AddAndUpdateActivityForm;
import se331.lab.rest.entity.form.EnrollActForm;

import java.util.List;

@Service
@Slf4j
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    ActivityDao activityDao;

    @Autowired
    TeacherService teacherService;

    @Autowired
    StudentDao studentDao;

    @Override
    public List<Activity> getAllActivity() {
        log.info("activity service received called");
        List<Activity> activities = activityDao.getAllActivity();
        log.info("activity service received {} \n from dao", activities);
        return activities;
    }

    @Override
    public Activity getActivityById(Long id) {
        log.info("activity service received called");
        Activity activity = activityDao.getActivityById(id);
        log.info("activity service received {} \n from dao", activity);
        return activity;
    }

    @Override
    public Activity saveActivity(AddAndUpdateActivityForm actForm) {
        log.info("activity service received called");
        Activity activity = Activity.builder()
                .name(actForm.getName())
                .location(actForm.getLocation())
                .description(actForm.getDescription())
                .startRegisDate(actForm.getStartRegisDate())
                .endRegisDate(actForm.getEndRegisDate())
                .startActDate(actForm.getStartActDate())
                .endActDate(actForm.getEndActDate())
                .startTime(actForm.getStartTime())
                .endTime(actForm.getEndTime())
                .host(teacherService.getTeacherById(actForm.getHostId()))
                .build();
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity updateActivity(AddAndUpdateActivityForm actForm) {
        log.info("activity service received called");
        Activity activity = activityDao.getActivityById(actForm.getActivityId());
        activity.setName(actForm.getName());
        activity.setLocation(actForm.getLocation());
        activity.setDescription(actForm.getDescription());
        activity.setStartRegisDate(actForm.getStartRegisDate());
        activity.setEndRegisDate(actForm.getEndRegisDate());
        activity.setStartActDate(actForm.getStartActDate());
        activity.setEndActDate(actForm.getEndActDate());
        activity.setStartTime(actForm.getStartTime());
        activity.setEndTime(actForm.getEndTime());
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity enrollActivity(EnrollActForm form) {
        Activity activity = activityDao.getActivityById(form.getActivityId());
        activity.getStudentsPending().add(studentDao.getStudentById(form.getStudentId()));
        int index = findIndexOfStudent(activity.getStudentsReject(), form.getStudentId());
        if (index != -1) {
            activity.getStudentsReject().remove(index);
        }
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity confirmEnrollActivity(EnrollActForm form) {
        Activity activity = activityDao.getActivityById(form.getActivityId());
        int index = findIndexOfStudent(activity.getStudentsPending(), form.getStudentId());
        activity.getStudentsPending().remove(index);
        activity.getStudentsEnrolled().add(studentDao.getStudentById(form.getStudentId()));
        return activityDao.saveActivity(activity);
    }

    @Override
    public Activity rejectEnrollActivity(EnrollActForm form) {
        Activity activity = activityDao.getActivityById(form.getActivityId());
        int index = findIndexOfStudent(activity.getStudentsPending(), form.getStudentId());
        activity.getStudentsPending().remove(index);
        activity.getStudentsReject().add(studentDao.getStudentById(form.getStudentId()));
        return activityDao.saveActivity(activity);
    }

    private int findIndexOfStudent(List<Student> students, Long id) {
        int index = -1;
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getId() == id) {
                index = i;
                break;
            }
        }
        return index;
    }
}
