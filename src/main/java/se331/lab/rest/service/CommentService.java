package se331.lab.rest.service;

import se331.lab.rest.entity.Comment;
import se331.lab.rest.entity.form.AddCommentForm;

import java.util.List;

public interface CommentService {
    List<Comment> getAllComment();

    Comment getCommentById(Long id);

    Comment saveComment(AddCommentForm commentForm);

    String removeComment(Long id);
}
