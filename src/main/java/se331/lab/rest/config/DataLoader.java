package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import se331.lab.rest.entity.*;
import se331.lab.rest.repository.*;
import se331.lab.rest.security.entity.Authority;
import se331.lab.rest.security.entity.AuthorityName;
import se331.lab.rest.security.entity.User;
import se331.lab.rest.security.repository.AuthorityRepository;
import se331.lab.rest.security.repository.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    AdminRepository adminRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        // Admin
        Admin admin = Admin.builder()
                .name("CAMT")
                .surname("_CMU")
                .build();

        admin = this.adminRepository.save(admin);

        // Student
        Student student1 = Student.builder()
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .dob("1998-01-06T17:00:00.000Z")
                .major("SE")
                .year("2nd")
                .email("Prayuth@email.com")
                .confirmed(true)
                .build();
        Student student2 = Student.builder()
                .studentId("SE-002")
                .name("Cherprang")
                .surname("BNK48")
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
                .dob("1998-07-11T17:00:00.000Z")
                .major("SE")
                .year("4th")
                .email("Cherprang@email.com")
                .confirmed(true)
                .build();
        Student student3 = Student.builder()
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                .dob("1998-11-05T17:00:00.000Z")
                .major("ANI")
                .year("2nd")
                .email("Nobi@email.com")
                .confirmed(false)
                .build();

        student1 = studentRepository.save(student1);
        student2 = studentRepository.save(student2);
        student3 = studentRepository.save(student3);

        // Teacher
        Teacher teacher1 = Teacher.builder()
                .name("Chartchai")
                .surname("Doungsa-ard")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-525f5.appspot.com/o/teacher01.jpg?alt=media&token=4cc25f76-cf1a-4746-9ef7-8eb7e0d46b29")
                .build();
        Teacher teacher2 = Teacher.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .build();

        teacher1 = this.teacherRepository.save(teacher1);
        teacher2 = this.teacherRepository.save(teacher2);

        // Activity
        Activity activity1 = Activity.builder()
                .name("Trekking Doisuthep")
                .location("Doisuthep")
                .description("Traditional CMU activity")
                .startRegisDate("2019-07-31T17:00:00.000Z")
                .endRegisDate("2019-08-06T17:00:00.000Z")
                .startActDate("2019-08-13T17:00:00.000Z")
                .endActDate("2019-08-13T17:00:00.000Z")
                .startTime("05.00 a.m.")
                .endTime("06.00 p.m.")
                .build();
        Activity activity2 = Activity.builder()
                .name("CMU Sport Day")
                .location("CMU main stadium")
                .description("Sport and exercise")
                .startRegisDate("2019-08-14T17:00:00.000Z")
                .endRegisDate("2019-08-29T17:00:00.000Z")
                .startActDate("2019-09-19T17:00:00.000Z")
                .endActDate("2019-09-19T17:00:00.000Z")
                .startTime("08.00 a.m.")
                .endTime("04.00 p.m.")
                .build();
        Activity activity3 = Activity.builder()
                .name("CMU Marathon")
                .location("Aung Kaew")
                .description("The bigest marathon in Chiang Mai University")
                .startRegisDate("2019-12-31T17:00:00.000Z")
                .endRegisDate("2020-01-04T17:00:00.000Z")
                .startActDate("2020-01-13T17:00:00.000Z")
                .endActDate("2020-01-13T17:00:00.000Z")
                .startTime("05.00 a.m.")
                .endTime("02.00 p.m.")
                .build();

        activity1 = this.activityRepository.save(activity1);
        activity2 = this.activityRepository.save(activity2);
        activity3 = this.activityRepository.save(activity3);

        // Comment
        Comment comment1 = Comment.builder()
                .message("This is very hard")
                .timestamp("2020-01-04T17:00:00.000Z")
                .build();
        Comment comment2 = Comment.builder()
                .message("Super easy!")
                .timestamp("2020-01-04T17:00:00.000Z")
                .image("https://firebasestorage.googleapis.com/v0/b/imageupload-525f5.appspot.com/o/Taka_Shiba.jpg?alt=media&token=23e7063b-75d6-4bf2-846f-eb720d20a644")
                .build();

        comment1 = this.commentRepository.save(comment1);
        comment2 = this.commentRepository.save(comment2);

        // add relationship
        comment1.setAuthor(student1);
        student1.getComments().add(comment1);
        comment1.setActivity(activity1);
        activity1.getComments().add(comment1);

        comment2.setAuthor(teacher1);
        teacher1.getComments().add(comment2);
        comment2.setActivity(activity1);
        activity1.getComments().add(comment2);

        student1.getEnrolledActivities().add(activity1);
        activity1.getStudentsEnrolled().add(student1);
        student1.getEnrolledActivities().add(activity3);
        activity3.getStudentsEnrolled().add(student1);
        student1.getPendingActivities().add(activity2);
        activity2.getStudentsPending().add(student1);

        teacher1.getActivities().add(activity1);
        activity1.setHost(teacher1);
        teacher1.getActivities().add(activity2);
        activity2.setHost(teacher1);
        teacher2.getActivities().add(activity3);
        activity3.setHost(teacher2);

        // add user
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        Authority authTeacher = Authority.builder()
                .name(AuthorityName.ROLE_TEACHER)
                .build();
        Authority authStudent = Authority.builder()
                .name(AuthorityName.ROLE_STUDENT)
                .build();
        Authority authAdmin = Authority.builder()
                .name(AuthorityName.ROLE_ADMIN)
                .build();

        User userAdmin, userTeacher1, userTeacher2, userStudent1, userStudent2, userStudent3;
        userAdmin = User.builder()
                .username("admin")
                .password(encoder.encode("admin1"))
                .firstname("CAMT")
                .lastname("_CMU")
                .email("camt@email.com")
                .enabled(true)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();

        // teacher
        userTeacher1 = User.builder()
                .username("teacher1")
                .password(encoder.encode("teacher1"))
                .firstname("Chartchai")
                .lastname("Doungsa-ard")
                .email("chartchai@email.com")
                .enabled(true)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();
        userTeacher2 = User.builder()
                .username("teacher2")
                .password(encoder.encode("teacher2"))
                .firstname("Jayakrit")
                .lastname("Hirisajja")
                .email("jayakrit@email.com")
                .enabled(true)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();

        // student
        userStudent1 = User.builder()
                .username("student1")
                .password(encoder.encode("123456"))
                .firstname("Prayuth")
                .lastname("The minister")
                .email("prayuth@email.com")
                .enabled(true)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();
        userStudent2 = User.builder()
                .username("student2")
                .password(encoder.encode("Cherprang"))
                .firstname("Cherprang")
                .lastname("BNK48")
                .email("cherprang@email.com")
                .enabled(true)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();
        userStudent3 = User.builder()
                .username("student3")
                .password(encoder.encode("nobinobi"))
                .firstname("Nobi")
                .lastname("Nobita")
                .email("Nobi@email.com")
                .enabled(false)
                .lastPasswordResetDate((Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant())))
                .build();

        authorityRepository.save(authAdmin);
        authorityRepository.save(authTeacher);
        authorityRepository.save(authStudent);

        userAdmin.getAuthorities().add(authAdmin);
        userTeacher1.getAuthorities().add(authTeacher);
        userTeacher2.getAuthorities().add(authTeacher);
        userStudent1.getAuthorities().add(authStudent);
        userStudent2.getAuthorities().add(authStudent);
        userStudent3.getAuthorities().add(authStudent);
        userRepository.save(userAdmin);
        userRepository.save(userTeacher1);
        userRepository.save(userTeacher2);
        userRepository.save(userStudent1);
        userRepository.save(userStudent2);
        userRepository.save(userStudent3);

        admin.setUser(userAdmin);
        userAdmin.setAppUser(admin);
        teacher1.setUser(userTeacher1);
        userTeacher1.setAppUser(teacher1);
        teacher2.setUser(userTeacher2);
        userTeacher2.setAppUser(teacher2);
        student1.setUser(userStudent1);
        userStudent1.setAppUser(student1);
        student2.setUser(userStudent2);
        userStudent2.setAppUser(student2);
        student3.setUser(userStudent3);
        userStudent3.setAppUser(student3);
    }
}
