package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Comment;
import se331.lab.rest.repository.CommentRepository;

import java.util.List;

@Repository
@Slf4j
public class CommentDBDaoImpl implements CommentDao {
    @Autowired
    CommentRepository commentRepository;

    @Override
    public List<Comment> getAllComment() {
        log.info("find all comment in db");
        return commentRepository.findAll();
    }

    @Override
    public Comment getCommentById(Long id) {
        log.info("find comment id {} in db", id);
        return commentRepository.findById(id).orElse(null);
    }

    @Override
    public Comment saveComment(Comment comment) {
        return commentRepository.save(comment);
    }

    @Override
    public void removeComment(Long id) {
        commentRepository.deleteById(id);
    }
}
