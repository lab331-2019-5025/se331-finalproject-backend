package se331.lab.rest.dao;

import se331.lab.rest.entity.Teacher;

import java.util.List;

public interface TeacherDao {
    List<Teacher> getAllTeacher();

    Teacher getTeacherById(Long id);

    Teacher saveTeacher(Teacher teacher);
}
