package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Admin;
import se331.lab.rest.repository.AdminRepository;

import java.util.List;

@Repository
@Slf4j
public class AdminDBDaoImpl implements AdminDao {
    @Autowired
    AdminRepository adminRepository;

    @Override
    public List<Admin> getAllAdmin() {
        log.info("find all admin in db");
        return adminRepository.findAll();
    }

    @Override
    public Admin getAdminById(Long id) {
        log.info("find admin id {} in db", id);
        return adminRepository.findById(id).orElse(null);
    }

    @Override
    public Admin saveAdmin(Admin admin) {
        log.info("save admin to database");
        return adminRepository.save(admin);
    }
}
