package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Teacher;
import se331.lab.rest.repository.TeacherRepository;

import java.util.List;

@Repository
@Slf4j
public class TeacherDBDaoImpl implements TeacherDao {
    @Autowired
    TeacherRepository teacherRepository;

    @Override
    public List<Teacher> getAllTeacher() {
        log.info("find all teacher in db");
        return teacherRepository.findAll();
    }

    @Override
    public Teacher getTeacherById(Long id) {
        log.info("find teacher id {} in db", id);
        return teacherRepository.findById(id).orElse(null);
    }

    @Override
    public Teacher saveTeacher(Teacher teacher) {
        log.info("save teacher to database");
        return teacherRepository.save(teacher);
    }
}
