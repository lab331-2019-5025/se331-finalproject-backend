package se331.lab.rest.dao;

import se331.lab.rest.entity.Activity;

import java.util.List;

public interface ActivityDao {
    List<Activity> getAllActivity();

    Activity getActivityById(Long id);

    Activity saveActivity(Activity activity);
}
