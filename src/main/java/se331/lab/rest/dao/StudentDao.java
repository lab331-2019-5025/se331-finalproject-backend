package se331.lab.rest.dao;

import se331.lab.rest.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getAllStudent();

    Student getStudentById(Long id);

    Student saveStudent(Student student);

    String removeStudent(Long id);
}
