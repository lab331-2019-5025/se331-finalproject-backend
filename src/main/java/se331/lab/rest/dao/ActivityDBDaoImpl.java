package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Activity;
import se331.lab.rest.repository.ActivityRepository;

import java.util.List;

@Repository
@Slf4j
public class ActivityDBDaoImpl implements ActivityDao {
    @Autowired
    ActivityRepository activityRepository;

    @Override
    public List<Activity> getAllActivity() {
        log.info("find all activity in db");
        return activityRepository.findAll();
    }

    @Override
    public Activity getActivityById(Long id) {
        log.info("find activity id {} in db", id);
        return activityRepository.findById(id).orElse(null);
    }

    @Override
    public Activity saveActivity(Activity activity) {
        return activityRepository.save(activity);
    }
}
