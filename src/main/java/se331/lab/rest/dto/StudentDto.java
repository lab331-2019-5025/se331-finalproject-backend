package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import se331.lab.rest.security.entity.User;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    Long id;
    String name;
    String surname;
    String image;
    String studentId;
    String dob;
    String major;
    String year;
    String email;
    Boolean confirmed;
    UserDataDto user;
}
