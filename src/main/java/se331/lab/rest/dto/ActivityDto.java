package se331.lab.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto {
    Long id;
    String name;
    String location;
    String description;
    String startRegisDate;
    String endRegisDate;
    String startActDate;
    String endActDate;
    String startTime;
    String endTime;
    HostDto host;

    @Builder.Default
    List<StudentDto> studentsEnrolled = new ArrayList<>();

    @Builder.Default
    List<StudentDto> studentsPending = new ArrayList<>();

    @Builder.Default
    List<StudentDto> studentsReject = new ArrayList<>();

    @Builder.Default
    List<CommentDto> comments = new ArrayList<>();
}
