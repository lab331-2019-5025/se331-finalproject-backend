package se331.lab.rest.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import se331.lab.rest.dto.*;
import se331.lab.rest.entity.*;
import se331.lab.rest.security.dto.UserDto;

import java.util.List;

@Mapper
public interface MapperUtil {
    MapperUtil INSTANCE = Mappers.getMapper(MapperUtil.class);

    @Mappings({})
    ActivityDto getActivityDto(Activity activity);

    @Mappings({})
    List<ActivityDto> getActivityDto(List<Activity> activities);

    @Mappings({})
    AdminDto getAdminDto(Admin admin);

    @Mappings({})
    List<AdminDto> getAdminDto(List<Admin> admins);

    @Mappings({})
    CommentDto getCommentDto(Comment comment);

    @Mappings({})
    List<CommentDto> getCommentDto(List<Comment> comments);

    @Mappings({})
    StudentDto getStudentDto(Student student);

    @Mappings({})
    List<StudentDto> getStudentDto(List<Student> students);

    @Mappings({})
    TeacherDto getTeacherDto(Teacher teacher);

    @Mappings({})
    List<TeacherDto> getTeacherDto(List<Teacher> teachers);

    @Mappings({})
    AuthorDto getAuthorDto(Person author);

    @Mappings({})
    List<AuthorDto> getAuthorDto(List<Person> author);

    @Mappings({})
    HostDto getHostDto(Teacher teacher);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "username", source = "user.username")
    })
    UserDto getUserDto(Student student);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "username", source = "user.username")
    })
    UserDto getUserDto(Teacher lecturer);

    @Mappings({
            @Mapping(target = "authorities", source = "user.authorities"),
            @Mapping(target = "username", source = "user.username")
    })
    UserDto getUserDto(Admin admin);
}
