package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.form.AddStudentForm;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.StudentService;

@Controller
@Slf4j
public class StudentController {

    @Autowired
    StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity getAllStudent() {
        log.info("student controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getAllStudent()));
    }

    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        log.info("student controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getStudentById(id)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody AddStudentForm stuForm) {
        log.info("student controller save student is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.saveStudent(stuForm)));
    }

    @PostMapping("/rejectRegisStudent/{id}")
    public ResponseEntity removeStudent(@PathVariable("id") Long id) {
        log.info("student controller delete student is call");
        return ResponseEntity.ok(studentService.removeStudent(id));
    }

    @PostMapping("/confirmRegisStudent/{id}")
    public ResponseEntity confirmRegisStudent(@PathVariable("id") Long id) {
        log.info("student controller confirm student is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.confirmRegisStudent(id)));
    }
}
