package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.form.AddCommentForm;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CommentService;

@Controller
@Slf4j
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping("/comments")
    public ResponseEntity getAllStudent() {
        log.info("comment controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.getAllComment()));
    }

    @GetMapping("/comments/{id}")
    public ResponseEntity getCommentById(@PathVariable("id") Long id) {
        log.info("comment controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.getCommentById(id)));
    }

    @PostMapping("/comments")
    public ResponseEntity saveComment(@RequestBody AddCommentForm commentForm) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCommentDto(commentService.saveComment(commentForm)));
    }

    @PostMapping("/removeComment/{id}")
    public ResponseEntity removeComment(@PathVariable("id") Long id) {
        return ResponseEntity.ok(commentService.removeComment(id));
    }
}
