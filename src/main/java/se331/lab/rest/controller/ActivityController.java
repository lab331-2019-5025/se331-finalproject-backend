package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.form.AddAndUpdateActivityForm;
import se331.lab.rest.entity.form.EnrollActForm;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.ActivityService;

@Controller
@Slf4j
public class ActivityController {
    @Autowired
    ActivityService activityService;

    @GetMapping("/activities")
    public ResponseEntity getAllActivity() {
        log.info("activity controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getAllActivity()));
    }

    @GetMapping("/activities/{id}")
    public ResponseEntity getActivityById(@PathVariable("id") Long id) {
        log.info("activity controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.getActivityById(id)));
    }

    @PostMapping("/activities")
    public ResponseEntity saveActivity(@RequestBody AddAndUpdateActivityForm actForm) {
        return ResponseEntity.ok(activityService.saveActivity(actForm));
    }

    @PostMapping("/updateActivity")
    public ResponseEntity updateActivity(@RequestBody AddAndUpdateActivityForm actForm) {
        log.info("update activity controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.updateActivity(actForm)));
    }

    @PostMapping("/enrollActivity")
    public ResponseEntity enrollActivity(@RequestBody EnrollActForm form) {
        log.info("student enroll activity controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.enrollActivity(form)));
    }

    @PostMapping("/confirmEnrollActivity")
    public ResponseEntity confirmEnrollActivity(@RequestBody EnrollActForm form) {
        log.info("confirm student enroll activity controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.confirmEnrollActivity(form)));
    }

    @PostMapping("/rejectEnrollActivity")
    public ResponseEntity rejectEnrollActivity(@RequestBody EnrollActForm form) {
        log.info("reject student enroll activity controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getActivityDto(activityService.rejectEnrollActivity(form)));
    }
}
