package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.form.UpdateProfileForm;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.AdminService;
import se331.lab.rest.service.StudentService;
import se331.lab.rest.service.TeacherService;

@Controller
@Slf4j
public class UpdateProfileController {
    @Autowired
    AdminService adminService;

    @Autowired
    StudentService studentService;

    @Autowired
    TeacherService teacherService;

    @PostMapping("/updateProfile")
    public ResponseEntity saveActivity(@RequestBody UpdateProfileForm updateProfileForm) {
        log.info("update profile controller is called");
        if (updateProfileForm.getRole().equalsIgnoreCase("ROLE_ADMIN")) {
            return ResponseEntity.ok(MapperUtil.INSTANCE.getUserDto(adminService.updateProfile(updateProfileForm)));
        } else if (updateProfileForm.getRole().equalsIgnoreCase("ROLE_STUDENT")) {
            return ResponseEntity.ok(MapperUtil.INSTANCE.getUserDto(studentService.updateProfile(updateProfileForm)));
        } else if (updateProfileForm.getRole().equalsIgnoreCase("ROLE_TEACHER")) {
            return ResponseEntity.ok(MapperUtil.INSTANCE.getUserDto(teacherService.updateProfile(updateProfileForm)));
        }
        return new ResponseEntity<>("Don't have this user in the system", HttpStatus.BAD_REQUEST);
    }
}
