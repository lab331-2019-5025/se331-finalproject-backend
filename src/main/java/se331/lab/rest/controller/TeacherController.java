package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.TeacherService;

@Controller
@Slf4j
public class TeacherController {
    @Autowired
    TeacherService teacherService;

    @GetMapping("/teachers")
    public ResponseEntity getAllTeacher() {
        log.info("teacher controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getTeacherDto(teacherService.getAllTeacher()));
    }

    @GetMapping("/teachers/{id}")
    public ResponseEntity getTeacherById(@PathVariable("id") Long id) {
        log.info("teacher controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getTeacherDto(teacherService.getTeacherById(id)));
    }
}
