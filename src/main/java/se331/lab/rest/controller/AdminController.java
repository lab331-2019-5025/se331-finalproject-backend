package se331.lab.rest.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.AdminService;

@Controller
@Slf4j
public class AdminController {
    @Autowired
    AdminService adminService;

    @GetMapping("/admins")
    public ResponseEntity getAllStudent() {
        log.info("admin controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getAdminDto(adminService.getAllAdmin()));
    }

    @GetMapping("/admins/{id}")
    public ResponseEntity getAdminById(@PathVariable("id") Long id) {
        log.info("admin controller is call");
        return ResponseEntity.ok(MapperUtil.INSTANCE.getAdminDto(adminService.getAdminById(id)));
    }
}
