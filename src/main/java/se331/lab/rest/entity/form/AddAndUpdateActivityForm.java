package se331.lab.rest.entity.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddAndUpdateActivityForm {
    Long activityId;
    String name;
    String location;
    String description;
    String startRegisDate;
    String endRegisDate;
    String startActDate;
    String endActDate;
    String startTime;
    String endTime;
    Long hostId;
}
