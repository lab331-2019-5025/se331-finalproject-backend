package se331.lab.rest.entity.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddStudentForm {
    String name;
    String surname;
    String dob;
    String studentId;
    String major;
    String year;
    String email;
    String image;
    String username;
    String password;
    String confirmPassword;
}
