package se331.lab.rest.entity.form;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProfileForm {
    Long appUserId;
    String newPassword;
    String image;
    String role;
}
