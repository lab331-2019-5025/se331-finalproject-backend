package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Student extends Person {
    String studentId;
    String dob;
    String major;
    String year;
    @Size(min = 4, max = 50)
    String email;
    Boolean confirmed;

    @ManyToMany(mappedBy = "studentsEnrolled")
    @Builder.Default
    @ToString.Exclude
    List<Activity> enrolledActivities = new ArrayList<>();

    @ManyToMany(mappedBy = "studentsPending")
    @Builder.Default
    @ToString.Exclude
    List<Activity> pendingActivities = new ArrayList<>();

    @ManyToMany(mappedBy = "studentsReject")
    @Builder.Default
    @ToString.Exclude
    List<Activity> rejectActivities = new ArrayList<>();

    @OneToMany(mappedBy = "author")
    @Builder.Default
    @JsonManagedReference
    @ToString.Exclude
    List<Comment> comments = new ArrayList<>();
}
