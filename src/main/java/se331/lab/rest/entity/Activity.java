package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String name;
    String location;
    String description;
    String startRegisDate;
    String endRegisDate;
    String startActDate;
    String endActDate;
    String startTime;
    String endTime;

    @ManyToOne
    @JsonBackReference
    Teacher host;

    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    List<Student> studentsEnrolled = new ArrayList<>();

    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    List<Student> studentsPending = new ArrayList<>();

    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    List<Student> studentsReject = new ArrayList<>();

    @OneToMany(mappedBy = "activity")
    @Builder.Default
    @ToString.Exclude
    List<Comment> comments = new ArrayList<>();
}
