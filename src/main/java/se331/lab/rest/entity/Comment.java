package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    String message;
    String image;
    String timestamp;

    @ManyToOne
    @JsonBackReference
    @ToString.Exclude
    Activity activity;

    @ManyToOne
    @JsonBackReference
    @ToString.Exclude
    Person author;
}
