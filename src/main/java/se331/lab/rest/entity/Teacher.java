package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Teacher extends Person {
    @OneToMany(mappedBy = "host")
    @Builder.Default
    @JsonManagedReference
    @ToString.Exclude
    List<Activity> activities = new ArrayList<>();

    @OneToMany(mappedBy = "author")
    @Builder.Default
    @JsonManagedReference
    @ToString.Exclude
    List<Comment> comments = new ArrayList<>();
}
